# catlyn

A simplistic yet powerful plugin interface in concept.

Welcome to the repository for catlyn!

> What is catlyn?

Catlyn is a simple yet powerful plugin interface. In english, it is a minimal program that provides an interface for non standalone programs called plugins.

> What is a plugin?

Plugins are programs that have some dependency on or ability to be parsed by catlyn. They provide essentially commands known as functions. Plugins can be written in pretty much anything as long as it can be executed either independently or by another program. Shell scripts, JS, Python, C++, anything. All that really matters is the presence of a plugin file and if needed a corresponding directory.


> What is a plugin file?

Okay, listen up this part is important if you want to develop plugins. Plugin files are configuration files in INI format, to be interpreted by catlyn in order to provide the functions. The format goes as follows:

[General]
name=<plugin name>
icon=<plugin icon path if used>
functions=<number of functions used>

[Function1]
f1name=<name of function>
f1desc=<function description, displayed on hover>
f1exec=<command executed by function, usually starting a shell script or other program>
f1icon=<path to function icon if required>
